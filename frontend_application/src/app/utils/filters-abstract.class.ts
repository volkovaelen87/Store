import { HttpParams } from '@angular/common/http';

export class AbstractFilters {
  public getHttpParams() {
    let result = new HttpParams();
    Object.keys(this).forEach((key) => {
      if (this[ key ] !== '') {
        result = result.set(key, this[ key ]);
      }
    });
    return result;
  }
}
