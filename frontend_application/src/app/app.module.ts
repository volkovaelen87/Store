import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { appRoutingProviders, router } from './app.main.router';

import { MenuModule } from './menu/menu.module';
import { AppCommonModule } from './common/common.module';
import { DashboardModule } from "./main/dashboard/dashboard.module";
import { ComposeModule } from "./main/compose/compose.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    router,
    BrowserModule,
    HttpClientModule,
    AppCommonModule,
    MenuModule,
    DashboardModule,
    ComposeModule,
  ],
  exports: [appRoutingProviders],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
