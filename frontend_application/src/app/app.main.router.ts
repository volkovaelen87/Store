import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/summary',
    pathMatch: 'full'
  }
];

export const appRoutingProviders: any[] = [];

export const router: ModuleWithProviders = RouterModule.forRoot(appRoutes);
