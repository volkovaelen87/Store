import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpirationDashboardComponent } from './expiration-dashboard.component';

describe('ExpirationDashboardComponent', () => {
  let component: ExpirationDashboardComponent;
  let fixture: ComponentFixture<ExpirationDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpirationDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpirationDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
