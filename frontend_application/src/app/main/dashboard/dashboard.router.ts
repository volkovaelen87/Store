import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RootDashboardComponent } from './root/root.component';
import { SummaryDashboardComponent } from "./summary/summary-dashboard.component";
import { ExpirationDashboardComponent } from "./expiration/expiration-dashboard.component";

const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    redirectTo: '/dashboard/summary',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: RootDashboardComponent,
    children: [
      {
        path: 'summary',
        component: SummaryDashboardComponent
      },
      {
        path: 'expiration',
        component: ExpirationDashboardComponent
      }
    ]
  },
];

export const dashboardRoutingProviders: any[] = [];

export const dashboardRouter: ModuleWithProviders = RouterModule.forChild(dashboardRoutes);
