import { Component, OnInit } from '@angular/core';

import { RestService } from "../../../../common/request/rest.service";
import { ConfigurationService } from "../../../../common/configuration/configuration.service";


@Component({
  selector: 'dashboard-card-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  readonly TITLE_NO_EXP: string = 'You do not have requests now.';
  readonly TITLE_EXP: string = 'You have requests now';

  public title: string = 'Loading data';

  constructor(private http: RestService,
              public configuration: ConfigurationService) {
  }

  ngOnInit() {
    if (this.configuration.userSpecificData.requestsProducts !== null) {
      if (this.configuration.userSpecificData.requestsProducts.items.length > 0) {
        this.title = this.TITLE_EXP;
      } else {
        this.title = this.TITLE_NO_EXP;
      }
    }
  }

  public getRequestsNumber(): number {
    if (this.configuration.userSpecificData.requestsProducts !== null) {
      return this.configuration.userSpecificData.requestsProducts.items.length;
    }
    return 0;
  }

  public getClass(): string {
    let result = 'bg-light';
    if (this.configuration.userSpecificData.requestsProducts !== null) {
      if (this.configuration.userSpecificData.requestsProducts.items.length > 0) {
        result = 'bg-warning';
      } else {
        result = 'bg-success';
      }
    }

    return result;
  }

}


