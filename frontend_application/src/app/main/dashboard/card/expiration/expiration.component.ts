import { Component, OnInit } from '@angular/core';

import { RestService } from "../../../../common/request/rest.service";
import { ConfigurationService } from "../../../../common/configuration/configuration.service";


@Component({
  selector: 'dashboard-card-expiration',
  templateUrl: './expiration.component.html',
  styleUrls: ['./expiration.component.scss']
})
export class ExpirationComponent implements OnInit {
  readonly TITLE_NO_EXP: string = 'You do not have products which are going to be expired during three month.';
  readonly TITLE_EXP: string = 'You have products which are going to be expired during three month.';

  public title: string = 'Loading data';

  constructor(private http: RestService,
              public configuration: ConfigurationService) {
  }

  ngOnInit() {
    if (this.configuration.userSpecificData.expirationProducts !== null) {
      if (this.configuration.userSpecificData.expirationProducts.items.length > 0) {
        this.title = this.TITLE_EXP;
      } else {
        this.title = this.TITLE_NO_EXP;
      }
    }
  }

  public getExpirationNumber(): number {
    if (this.configuration.userSpecificData.expirationProducts !== null) {
      return this.configuration.userSpecificData.expirationProducts.items.length;
    }
    return 0;
  }

  public getClass(): string {
    let result = 'bg-light';
    if (this.configuration.userSpecificData.expirationProducts !== null) {
      if (this.configuration.userSpecificData.expirationProducts.items.length > 0) {
        result = 'bg-warning';
      } else {
        result = 'bg-success';
      }
    }

    return result;
  }

}
