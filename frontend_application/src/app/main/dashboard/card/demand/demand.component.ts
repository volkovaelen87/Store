import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from "../../../../common/configuration/configuration.service";
import { RestService } from "../../../../common/request/rest.service";


@Component({
  selector: 'dashboard-card-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.scss']
})

export class DemandComponent implements OnInit {
  readonly TITLE_NO_EXP: string = 'You do not have products that are not enough to satisfy your needs.';
  readonly TITLE_EXP: string = 'You do not have enough products, the amount of which is insufficient to satisfy your need.';

  public title: string = 'Loading data';

  constructor(private http: RestService,
              public configuration: ConfigurationService) {
  }

  ngOnInit() {
    if (this.configuration.userSpecificData.demandProducts !== null) {
      if (this.configuration.userSpecificData.demandProducts.items.length > 0) {
        this.title = this.TITLE_EXP;
      } else {
        this.title = this.TITLE_NO_EXP;
      }
    }
  }

  public getDemandNumber(): number {
    if (this.configuration.userSpecificData.demandProducts !== null) {
      return this.configuration.userSpecificData.demandProducts.items.length;
    }
    return 0;
  }

  public getClass(): string {
    let result = 'bg-light';
    if (this.configuration.userSpecificData.demandProducts !== null) {
      if (this.configuration.userSpecificData.demandProducts.items.length > 0) {
        result = 'bg-warning';
      } else {
        result = 'bg-success';
      }
    }

    return result;
  }

}
