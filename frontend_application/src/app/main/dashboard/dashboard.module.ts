import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppCommonModule } from '../../common/common.module';

import { dashboardRouter, dashboardRoutingProviders } from './dashboard.router';

import { RootDashboardComponent } from './root/root.component';
import { ExpirationComponent } from './card/expiration/expiration.component';
import { RequestsComponent } from './card/requests/requests.component';
import { DemandComponent } from './card/demand/demand.component';

import { ExpirationDashboardComponent } from "./expiration/expiration-dashboard.component";
import { SummaryDashboardComponent } from './summary/summary-dashboard.component';

@NgModule({
  imports: [
    AppCommonModule,
    dashboardRouter,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  exports: [RootDashboardComponent],
  declarations: [RootDashboardComponent, ExpirationComponent, RequestsComponent, DemandComponent, ExpirationDashboardComponent, SummaryDashboardComponent],
  providers: [dashboardRoutingProviders]
})
export class DashboardModule {
}
