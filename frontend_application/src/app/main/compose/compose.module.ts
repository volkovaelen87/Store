import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { RootComposeComponent } from './root-compose/root.component';

import { composeRouter, composeRoutingProviders } from "./compose.router";
import { RequestsComponent } from './request/requests.component';
import { ExternalWaybillComponent } from "./external-waybill/external-waybill.component";
import { InternalWaybillComponent } from './internal-waybill/internal-waybill.component';
import { InputComposeComponent } from "./input-compose/input-compose.component";
import { RequestProductComponent } from './request/request-product/request-product.component';
import { InputComposeInternalWaybillComponent } from "./input-compose-internal-waybill/input-compose-internal-waybill.component";


@NgModule({
  imports: [
    FormsModule,
    composeRouter,
    ReactiveFormsModule,
    CommonModule,
  ],
  exports: [RootComposeComponent],
  declarations: [
    RootComposeComponent, RequestsComponent, ExternalWaybillComponent, InternalWaybillComponent,
    InputComposeComponent, RequestProductComponent, InputComposeInternalWaybillComponent
  ],
  providers: [composeRoutingProviders]
})
export class ComposeModule {
}
