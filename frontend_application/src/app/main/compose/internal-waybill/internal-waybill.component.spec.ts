import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalWaybillComponent } from './internal-waybill.component';

describe('InternalWaybillComponent', () => {
  let component: InternalWaybillComponent;
  let fixture: ComponentFixture<InternalWaybillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalWaybillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalWaybillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
