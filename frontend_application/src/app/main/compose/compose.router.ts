import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RootComposeComponent } from "./root-compose/root.component";

import { RequestsComponent } from "./request/requests.component";
import { ExternalWaybillComponent } from "./external-waybill/external-waybill.component";
import { InternalWaybillComponent } from "./internal-waybill/internal-waybill.component";


const composeRoutes: Routes = [
  {
    path: 'compose',
    redirectTo: '/compose/requests',
    pathMatch: 'full'
  },
  {
    path: 'compose',
    component: RootComposeComponent,
    children: [
      {
        path: 'requests',
        component: RequestsComponent
      },
      {
        path: 'external-waybill',
        component: ExternalWaybillComponent
      },
      {
        path: 'internal-waybill',
        component: InternalWaybillComponent
      },
    ]
  },
];

export const composeRoutingProviders: any[] = [];

export const composeRouter: ModuleWithProviders = RouterModule.forChild(composeRoutes);
