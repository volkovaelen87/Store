import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputComposeInternalWaybillComponent } from "./input-compose-internal-waybill.component";

describe('InputActionsInternalWaybillComponent', () => {
  let component: InputComposeInternalWaybillComponent;
  let fixture: ComponentFixture<InputComposeInternalWaybillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputComposeInternalWaybillComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComposeInternalWaybillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
