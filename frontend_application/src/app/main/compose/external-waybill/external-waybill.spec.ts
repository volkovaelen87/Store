import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalWaybillComponent } from './external-waybill.component';

describe('DeliveryNoteComponent', () => {
  let component: ExternalWaybillComponent;
  let fixture: ComponentFixture<ExternalWaybillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExternalWaybillComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalWaybillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
