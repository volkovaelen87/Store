import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootComposeComponent } from './root.component';


describe('RootManagerComponent', () => {
  let component: RootComposeComponent;
  let fixture: ComponentFixture<RootComposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RootComposeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootComposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
