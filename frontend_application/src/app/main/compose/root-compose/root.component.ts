import { Component } from '@angular/core';

import { ConfigurationService } from '../../../common/configuration/configuration.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComposeComponent {
  constructor(public configuration: ConfigurationService) {
  }


}
