import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import 'rxjs/add/operator/debounceTime';

import { ProductModel } from "../../../../models/product.model";
import { RestService } from "../../../../common/request/rest.service";
import { NumberValidator } from "../../../../validators/type.validators";
import { GreaterNumberValidator } from "../../../../validators/number.validators";


function InListValidator(component): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (component.products.length === 0) return {listEmpty: true};
    return component.products.some(product => product.name === control.value) ? null : {noProduct: true};
  };
}

@Component({
  selector: 'request-product',
  templateUrl: './request-product.component.html',
  styleUrls: ['./request-product.component.scss']
})
export class RequestProductComponent implements OnInit {
  @Input()
  public form: FormGroup = null;

  @Output()
  public changed = new EventEmitter<boolean>();


  public products: ProductModel[] = [];

  constructor(private httpService: RestService) {
  }

  ngOnInit() {
    this.form.addControl('product', new FormControl('', [Validators.required, InListValidator(this)]));
    this.form.addControl('amount', new FormControl(0, [GreaterNumberValidator(0), NumberValidator]));

    this.form.controls.product.valueChanges.debounceTime(1500).subscribe(
      value => {
        if (value.length < 3) return;
        if (this.products.some(product => product.name === value)) return;
        this.httpService.get<ProductModel[]>('/api/products/', {params: {name: value}}).subscribe(
          result => {
            this.products = result;
            this.form.controls.product.updateValueAndValidity();
          }
        )
      });

    this.form.valueChanges.subscribe(() => this.changed.emit(this.form.valid));
  }

}
