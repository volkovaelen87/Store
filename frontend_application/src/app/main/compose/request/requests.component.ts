import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  public products: FormGroup[] = [];

  constructor() {
  }

  ngOnInit() {
    this.products.push(new FormGroup({}));
  }

  productChanged(event) {
    let notValidAmount = 0;
    let emptyProducts = 0;
    this.products.forEach(productForm => {
      if (!productForm.valid) {
        ++notValidAmount;
      }
      if (productForm.controls.product.value.length === 0) {
        ++emptyProducts;
      }
    });

    if (notValidAmount === 0) {
      // Add product input if all products are valid
      this.products.push(new FormGroup({}));
    }

    if (emptyProducts > 1) {
      // Remove all products with empty name except last
      let to_remove = emptyProducts - 1;
      let new_products = [];
      this.products.forEach(product => {
        if (product.controls.product.value.length !== 0) {
          new_products.push(product);
        } else if (to_remove > 0) {
          --to_remove;
        } else {
          new_products.push(product);
        }
      });
      this.products = new_products;
    }

  }

}
