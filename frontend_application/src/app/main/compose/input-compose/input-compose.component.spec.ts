import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputComposeComponent } from './input-compose.component';

describe('InputActionsComponent', () => {
  let component: InputComposeComponent;
  let fixture: ComponentFixture<InputComposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputComposeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
