import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { RestService } from './common/request/rest.service';
import { ConfigurationService } from './common/configuration/configuration.service';

import { GoogleLoginService } from './menu/authentication/login/login-services/google.service';

import { User } from './menu/authentication/login/login-services/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
  title = 'app';
  subscription: Subscription;

  constructor(private http: RestService,
              private configuration: ConfigurationService,
              private loginService: GoogleLoginService) {
  }

  ngOnInit(): void {
    const email = localStorage.getItem('email');
    let url = '/api/authentication/';
    if (email !== null) {
      url += '?email=' + email;
    }

    this.updateMetadata();

    this.http.get<User>(url).subscribe(
      (userData) => {
        if (userData !== null) {
          this.loginService.signIn();
        }
      });

    this.subscription = this.configuration.authenticated$.subscribe(() => this.updateMetadata(true));
  }

  private updateMetadata(force = true): void {
    if (this.initialized() && !force) {
      return;
    }
  }

  private initialized() {
  }
}
