import { ProductModel } from "./product.model";
import { StorageAmountModel } from "./storage.model";

export class ExpirationShipmentModel {
  id: number;
  product: ProductModel;
  amount: number;
  per_storage_amount: StorageAmountModel[];
  serial: string;
  made_date: string;
  expiration_date: string;
  item_cost: number;
}

export class ExpirationModel {
  items: ExpirationShipmentModel[] = [];
}
