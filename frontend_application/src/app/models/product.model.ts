import { CategoryFullModel } from "./category.model";
import { ConcentrationFullModel } from "./concentration.model";

export class ProductModel {
  id: number;
  name: string;
  category: CategoryFullModel[];
  concentration_type: ConcentrationFullModel;
  concentration: number;
  details: string;
  packaging: number;
  package_amount: number;
}
