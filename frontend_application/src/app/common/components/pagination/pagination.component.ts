import { Component, OnInit, OnDestroy, OnChanges, EventEmitter, Input, Output } from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'common-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: [ './pagination.component.scss' ]
})
export class PaginationComponent implements OnInit, OnDestroy, OnChanges {
  private readonly SIMPLE_VIEW_COUNT = 5;

  @Input('count') public resultsCount: number = 0;
  @Input('displayed') public resultsDisplayed: number = 0;
  @Input('nextPage') public nextPageUrl: string = null;
  @Input('previousPage') public previousPageUrl: string = null;

  @Output() public loadPageUrl = new EventEmitter<string>();

  private complexViewPageIdChangedSubject = new Subject<number>();
  private inputChanged$ = this.complexViewPageIdChangedSubject.asObservable();
  private inputChangedSubscription: Subscription;

  public itemsPerPage: number = null;
  public pageCount: number = 0;

  public currentPage: number = 0;
  public elements: string[] = [];

  public complexViewPageInputValue;
  private complexViewPageInputValueSave;

  constructor() {
  }

  ngOnInit() {
    this.inputChangedSubscription = this.inputChanged$.debounceTime(500).subscribe(pageId => this.navigateToPage(pageId));
  }

  ngOnDestroy() {
    this.inputChangedSubscription.unsubscribe();
  }

  ngOnChanges(changes) {
    Object.entries(changes).forEach(([ key, value ]) => this[ key ] = value['currentValue']);
    this.updateItemsPerPage();

    if (this.resultsDisplayed > 0 && this.resultsCount > this.resultsDisplayed) {
      const newPageCount = Math.floor(this.resultsCount / this.itemsPerPage) + 1;
      if (newPageCount !== this.pageCount) {
        this.pageCount = newPageCount;
        this.updateView();
      }
    } else {
      if (this.pageCount !== 0) {
        this.pageCount = 0;
        this.updateView();
      }
    }
    this.updateCurrentPage();
  }

  public navigateToPage(pageId: number): void {
    if (pageId === this.currentPage) {
      return;
    }

    let url = this.previousPageUrl;
    if (url === null) {
      url = this.nextPageUrl;
    }

    if (url !== null) {
      const resultUrl = new URL(url.split('?')[ 0 ]);
      resultUrl.searchParams.set('page', pageId.toString());
      this.loadPageUrl.emit(resultUrl.toString());
    }
  }

  public inputFocus() {
    this.complexViewPageInputValueSave = this.complexViewPageInputValue;
    this.complexViewPageInputValue = '';
  }

  public inputFocusOut() {
    const value = parseInt(this.complexViewPageInputValue, 10);
    console.log('val', value);
    if (Object.is(value, NaN)) {
      this.complexViewPageInputValue = this.complexViewPageInputValueSave;
    } else {
      if (value > 0 && value <= this.pageCount) {
        this.complexViewPageInputValue = value;
      }
    }
  }

  public complexViewPageInputChanged() {
    const value = parseInt(this.complexViewPageInputValue, 10);
    if (!Object.is(value, NaN) && value > 0 && value <= this.pageCount && this.currentPage !== value) {
      this.complexViewPageIdChangedSubject.next(value);
    }
  }

  public isSimple(): boolean {
    return this.pageCount <= this.SIMPLE_VIEW_COUNT;
  }

  private updateItemsPerPage() {
    if (this.itemsPerPage === null && this.nextPageUrl !== null) {
      this.itemsPerPage = this.resultsDisplayed;
    }
  }

  private updateCurrentPage() {
    let previousPageId: number = 0;
    if (this.previousPageUrl !== null) {
      const previousPageUrl = new URL(this.previousPageUrl);
      previousPageId = parseInt(previousPageUrl.searchParams.get('page'), 10);
      if (Object.is(previousPageId, NaN)) {
        previousPageId = 1;
      }
    }
    this.currentPage = ++previousPageId;
    this.complexViewPageInputValue = this.currentPage;
  }


  private updateView() {
    const newElements = [];
    for (let pageId = 1; pageId <= this.pageCount; ++pageId) {
      newElements.push(pageId);
    }
    this.elements = newElements;
  }
}
