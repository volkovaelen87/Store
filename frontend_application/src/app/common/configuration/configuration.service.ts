import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { RestService } from '../request/rest.service';

import { User } from '../../menu/authentication/login/login-services/user';
import { ExpirationModel } from "../../models/expiration.model";
import { RequestsModel } from "../../models/requests.model";
import { DemandModel } from "../../models/demand.model";


class ConfigurationDataModel {
  user: User = null;

  expirationProducts: ExpirationModel = null;
  requestsProducts: RequestsModel = null;
  demandProducts: DemandModel = null;
}

@Injectable()
export class ConfigurationService {
  private authenticated = new Subject<boolean>();
  authenticated$ = this.authenticated.asObservable();

  private backup;

  public userSpecificData: ConfigurationDataModel = new ConfigurationDataModel();

  constructor(private http: RestService,
              private _ngZone: NgZone) {
    this.backup = JSON.stringify(this.userSpecificData);
  }

  authenticate(user: User) {
    this.http.post('/api/authentication/', user).subscribe(
      () => {
        this._ngZone.run(() => {
          this.set_authentication_user(user);
        });
        localStorage.setItem('email', user.email);
      }
    );
  }

  public set_authentication_user(user: User) {
    this.userSpecificData.user = user;
    this.authenticated.next(true);
  }

  public user(): any {
    return this.userSpecificData.user;
  }

  public userName(): string {
    return this.userSpecificData.user.name;
  }

  public userEmail(): string {
    return this.userSpecificData.user.email;
  }

  public is_authenticated(): boolean {
    return this.userSpecificData.user !== null;
  }

  public logout(): void {
    this._ngZone.run(() => {
      this.userSpecificData = new ConfigurationDataModel();
      this.authenticated.next(false);
    });
  }
}
