import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';

import { ModalDialogComponent } from './dialog/modal.dialog.component';
import { BlockComponent } from './block/block.component';

import { RestService } from './request/rest.service';
import { ConfigurationService } from './configuration/configuration.service';
import { ModalDialogService } from './dialog/modal.dialog.service';
import { BlockService } from './block/block.service';
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    }),
  ],
  declarations: [ ModalDialogComponent, BlockComponent, PaginationComponent ],
  providers: [ RestService, ConfigurationService, ModalDialogService, BlockService ],
  exports: [ ModalDialogComponent, BlockComponent, PaginationComponent ]
})
export class AppCommonModule {
}
