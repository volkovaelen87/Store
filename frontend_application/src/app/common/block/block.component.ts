import { Component } from '@angular/core';

import { BlockService } from './block.service';

@Component({
  selector: 'window-block',
  templateUrl: './block.component.html',
  styleUrls: [ './block.component.scss' ]
})
export class BlockComponent {
  display: boolean = false;

  constructor(private blockService: BlockService) {
    blockService.block$.subscribe(
      state => {
        this.display = state;
      });
  }

}
