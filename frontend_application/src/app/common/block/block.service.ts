import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class BlockService {

  private blockSource = new Subject<boolean>();

  block$ = this.blockSource.asObservable();

  lock(state: boolean) {
    this.blockSource.next(state);
  }

}
