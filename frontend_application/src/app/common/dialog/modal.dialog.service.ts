import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ModalDialogService {
  constructor() {
  }

  private displaySource = new Subject<{
    display: boolean,
    properties?: {
      title?: string,
      topCloseButton?: boolean,
      body?: string,
      buttonDefaultName?: string,
      buttonPrimaryName?: string
    }
  }>();
  private resultSource = new Subject<boolean>();

  display$ = this.displaySource.asObservable();
  result$ = this.resultSource.asObservable();

  display(data: {
    display: boolean,
    properties?: {
      title?: string,
      topCloseButton?: boolean,
      body?: string,
      buttonDefaultName?: string,
      buttonPrimaryName?: string
    }
  }) {
    this.displaySource.next(data);
  }

  result(result: boolean) {
    this.resultSource.next(result);
  }

}
