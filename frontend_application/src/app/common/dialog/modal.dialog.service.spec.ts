import { TestBed, inject } from '@angular/core/testing';

import { ModalDialogService } from './modal.dialog.service';

describe('Modal.DialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ ModalDialogService ]
    });
  });

  it('should be created', inject([ ModalDialogService ], (service: ModalDialogService) => {
    expect(service).toBeTruthy();
  }));
});
