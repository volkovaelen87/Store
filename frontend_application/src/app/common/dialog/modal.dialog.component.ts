import { Component } from '@angular/core';

import { ModalDialogService } from './modal.dialog.service';
import { BlockService } from '../block/block.service';

@Component({
  selector: 'modal-dialog',
  templateUrl: './modal.dialog.component.html',
  styleUrls: [ './modal.dialog.component.scss' ]
})
export class ModalDialogComponent {
  display: boolean = false;
  title: string = '';
  topCloseButton: boolean = true;
  body: string = '';

  hasButtons: boolean = false;
  buttonDefaultName: string = '';
  buttonPrimaryName: string = '';

  properties_list = {
    title: '',
    topCloseButton: true,
    body: '',
    buttonDefaultName: 'Close',
    buttonPrimaryName: ''
  };

  constructor(private dialogService: ModalDialogService,
              private blockService: BlockService) {
    dialogService.display$.subscribe(
      data => {
        if (data.display) {

          for (const key of Object.keys(this.properties_list)) {
            if (key in data.properties) {
              this[ key ] = data.properties[ key ];
            } else {
              this[ key ] = this.properties_list[ key ];
            }
          }

          this.hasButtons = false;
          if (this.buttonDefaultName.length || this.buttonPrimaryName.length) {
            this.hasButtons = true;
          }

          this.blockService.lock(true);
          this.display = true;
        } else {
          this.display = false;
          this.blockService.lock(false);
        }
      });
  }

  confirm() {
    this.dialogService.result(true);
    this.display = false;
    this.blockService.lock(false);
  }

  close() {
    this.dialogService.result(false);
    this.display = false;
    this.blockService.lock(false);
  }

}
