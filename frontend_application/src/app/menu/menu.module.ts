import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AppCommonModule } from '../common/common.module';

import { MenuRootComponent } from './root/root.component';
import { MenuAuthenticationComponent } from './authentication/authentication.component';


import { GoogleLoginService } from './authentication/login/login-services/google.service';
import { User } from './authentication/login/login-services/user';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    AppCommonModule,
    RouterModule,
  ],
  declarations: [ MenuRootComponent, MenuAuthenticationComponent ],
  providers: [
    GoogleLoginService,
    User,
  ],
  exports: [ MenuRootComponent ]
})
export class MenuModule {
}
