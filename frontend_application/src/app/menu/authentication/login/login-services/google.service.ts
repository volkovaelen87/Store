import { Injectable } from '@angular/core';

import { User } from './user';

import { RestService } from '../../../../common/request/rest.service';
import { ConfigurationService } from '../../../../common/configuration/configuration.service';

import { LoginServiceInterface } from './login-service.interface';

declare let gapi: any;

@Injectable()
export class GoogleLoginService implements LoginServiceInterface {
  public readonly ISSUER_NAME = 'google';

  private readonly CLIENT_ID = '317829113035-jl901274cjgfe4c3874vm6kho3pc6or2.apps.googleusercontent.com';

  private auth2: any = null;

  constructor(private configuration: ConfigurationService,
              private http: RestService) {
  }

  private initialize(): Promise<null> {
    return new Promise((resolve) => {
      if (this.auth2 === null) {
        gapi.load('auth2', () => {
          this.auth2 = gapi.auth2.init({
            client_id: this.CLIENT_ID,
            scope: 'email',
          });
          resolve();
        });
      } else {
        resolve();
      }
    });
  }

  private processUser(): void {
    const user: User = new User();
    const profile = this.auth2.currentUser.get().getBasicProfile();
    const authResponseObj = this.auth2.currentUser.get().getAuthResponse(true);
    user.id = profile.getId();
    user.name = profile.getName();
    user.email = profile.getEmail();
    user.image = profile.getImageUrl();
    user.accessToken = authResponseObj.access_token;
    user.idToken = authResponseObj.id_token;
    user.provider = this.ISSUER_NAME;
    this.configuration.authenticate(user);
  }

  public signIn(): void {
    this.initialize().then(
      () => {
        this.auth2.then(
          () => {
            if (this.auth2.isSignedIn.get()) {
              this.processUser();
            } else {
              this.auth2.signIn({immediate: true}).then(() => {
                this.processUser();
              });
            }
          });
      },
      err => {
        console.log(err);
      }
    );
  }

  public signOut(): void {
    if (this.configuration.is_authenticated()) {
      this.initialize().then(
        () => {
          this.auth2.signOut().then(
            (err: any) => {
              if (err) {
                console.log('Unable to logout from Google');
              }
            });
        });
      this.http.delete('/api/authentication/').subscribe(
        () => {
          this.configuration.logout();
        });
    }
  }
}
