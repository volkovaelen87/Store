export interface LoginServiceInterface {
  signIn(): void;

  signOut(): void;
}
