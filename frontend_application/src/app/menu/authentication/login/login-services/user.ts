export class User {
  provider: any;
  id: string;
  email: string;
  name: string;
  image: string;
  accessToken: string;
  idToken: string;
}
