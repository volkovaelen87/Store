import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAuthenticationComponent } from './authentication.component';

describe('AuthenticationComponent', () => {
  let component: MenuAuthenticationComponent;
  let fixture: ComponentFixture<MenuAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAuthenticationComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
