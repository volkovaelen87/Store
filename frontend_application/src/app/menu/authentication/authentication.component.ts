import { Component } from '@angular/core';

import { ConfigurationService } from '../../common/configuration/configuration.service';

import { GoogleLoginService } from './login/login-services/google.service';

@Component({
  selector: 'menu-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: [ './authentication.component.scss' ]
})
export class MenuAuthenticationComponent {

  constructor(public configuration: ConfigurationService,
              private googleLogin: GoogleLoginService) {
  }

  public userFullName(): string {
    let result: string = '';
    if (this.configuration.is_authenticated()) {
      result = this.configuration.userName() + ' (' + this.configuration.userEmail() + ')';
    }
    return result;
  }

  public login() {
    this.googleLogin.signIn();
  }

  public logout() {
    localStorage.clear();
    this.googleLogin.signOut();
  }
}
