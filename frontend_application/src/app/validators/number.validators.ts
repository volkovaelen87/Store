import { AbstractControl } from "@angular/forms";

export function GreaterNumberValidator(num: number) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return control.value > num ? null : {valueIsLessOrEqual: true};
  };
}
