import { AbstractControl } from "@angular/forms";

export function NumberValidator(control: AbstractControl) {
  if (control.value === '') {
    return {empty: true};
  }
  return isNaN(control.value) ? {forbiddenType: true} : null;
}
