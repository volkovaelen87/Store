from django.urls import path

from core.views.authentication import AuthenticationView
from core.views.category import CategoryView, CategoriesView
from core.views.concentration_type import ConcentrationTypeView, ConcentrationTypesView
from core.views.department import DepartmentView, DepartmentsView
from core.views.expiration import ExpirationView
from core.views.item import ItemView, ItemsView
from core.views.motion import MotionView, MotionsView
from core.views.packaging import PackagingView, PackagingsView
from core.views.post import PostView, PostsView
from core.views.request import RequestView, RequestsView
from core.views.product import ProductView, ProductsView
from core.views.depot import DepotView, DepotsView
from core.views.transfer import TransferView, TransfersView
from core.views.user import UserView, UsersView

urlpatterns = [
    path("authentication/", AuthenticationView.as_view()),
    path("department/", DepartmentsView.as_view()),
    path("department/<int:department_id>/", DepartmentView.as_view()),

    path("product/", ProductsView.as_view()),
    path("product/<int:product_id>/", ProductView.as_view()),
    path("request/", RequestsView.as_view()),
    path("request/<int:request_id>/", RequestView.as_view()),

    path("packaging/", PackagingsView.as_view()),
    path("packaging/<int:packaging_id>/", PackagingView.as_view()),
    path("motion/", MotionsView.as_view()),
    path("motion/", MotionView.as_view()),
    path("motion/<int:motion_id>/", MotionView.as_view()),
    path("depot/", DepotsView.as_view()),
    path("depot/<int:depot_id>/", DepotView.as_view()),
    path("post/", PostsView.as_view()),
    path("post/<int:post_id>/", PostView.as_view()),
    path("transfer/", TransfersView.as_view()),
    path("transfer/<int:transfer_id>/", TransferView.as_view()),
    path("category/", CategoriesView.as_view()),
    path("category/<int:category_id>/", CategoryView.as_view()),
    path("concentration_type/", ConcentrationTypesView.as_view()),
    path("concentration_type/<int:concentration_type_id>/", ConcentrationTypeView.as_view()),
    path("item/", ItemsView.as_view()),
    path("item/<int:item_id>/", ItemView.as_view()),
    path("user/", UsersView.as_view()),
    path("user/<int:user_id>/", UserView.as_view()),
    path("expiration/", ExpirationView.as_view()),
]
