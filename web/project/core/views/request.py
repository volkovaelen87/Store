from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Request
from core.serializers.request import (RequestRetrieveSerializer, RequestCreateSerializer, RequestListSerializer,
                                      RequestUpdateSerializer)
from core.utils.serializer_mixin import SerializerMixin


class RequestsView(SerializerMixin, ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Request.objects.all()
    retrieve_serializer_class = RequestListSerializer
    create_serializer_class = RequestCreateSerializer


class RequestView(SerializerMixin, RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "request_id"
    queryset = Request.objects.all()
    retrieve_serializer_class = RequestRetrieveSerializer
    update_serializer_class = RequestUpdateSerializer
