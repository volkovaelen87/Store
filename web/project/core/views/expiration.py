from datetime import date, timedelta
from django.db.models import Q, Sum, QuerySet

from rest_framework import permissions
from rest_framework.generics import ListAPIView

from core.models import Depot, Item
from core.serializers.item import ItemDetailStorageSerializer


class ExpirationView(ListAPIView):
    EXPIRATION_NOTIFICATION_DAYS = 3 * 30

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ItemDetailStorageSerializer

    def get_queryset(self):
        storages = {
            storage.id: storage for storage in Depot.objects.filter(created_by=self.request.user)
        }

        queryset = QuerySet()
        if storages:
            queryset = Item.objects \
                .filter(expiration_date__lte=date.today() + timedelta(self.EXPIRATION_NOTIFICATION_DAYS)) \
                .prefetch_related("motion_set") \
                .annotate(amount=Sum("motion__change", filter=Q(motion__storage_id__in=storages.keys()))) \
                .filter(amount__gt=0)

            for storage_id, storage in storages.items():
                queryset = queryset.annotate(
                    **{"storage_{}".format(storage_id): Sum("motion__change", filter=Q(motion__storage_id=storage_id))}
                )
        return queryset
