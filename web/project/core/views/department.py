from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Department
from core.serializers.department import DepartmentSerializer


class DepartmentsView(ListAPIView,CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class DepartmentView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "department_id"
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
