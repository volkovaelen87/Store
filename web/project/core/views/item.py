from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Item
from core.serializers.item import ItemSerializer, ItemDetailSerializer
from core.utils.serializer_mixin import SerializerMixin


class ItemsView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Item.objects \
        .select_related("product", "product__packaging", "product__concentration_type") \
        .all()
    serializer_class = ItemDetailSerializer


class ItemView(SerializerMixin, RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "item_id"
    queryset = Item.objects \
        .select_related("product", "product__packaging", "product__concentration_type") \
        .all()
    serializer_class = ItemSerializer
    retrieve_serializer_class = ItemDetailSerializer
