import json
from django.middleware import csrf
from django.views.generic.base import View
from django.contrib.auth import login, logout
from django.http.response import HttpResponse, JsonResponse, HttpResponseBadRequest

from core.libs.redis_pool import RedisPool, RedisDB
from core.libs.authentication.token_issuer import UserInformation, TokenIssuer

from core.models import User


class AuthenticationView(View):
    http_method_names = ["get", "post", "delete"]

    def get(self, request, *args, **kwargs):
        csrf.get_token(request=request)
        user_email = request.GET.get("email")

        if user_email:
            redis = RedisPool()[RedisDB.DB_AUTH]
            user_data = redis.get(user_email)
            if user_data:
                try:
                    user = UserInformation(data=json.loads(user_data.decode("utf-8")))

                    issuer = TokenIssuer.get(name=user.provider)

                    if issuer.validator.validate(user=user):
                        django_user = User.objects.get(email=user_email)
                        login(request=request, user=django_user)
                        django_user.save()
                        return JsonResponse(user.data)
                except (json.JSONDecodeError, KeyError, User.DoesNotExist):
                    pass

                redis.delete(user_email)

        return JsonResponse({})

    def post(self, request, *args, **kwargs):
        try:
            user = UserInformation(data=json.loads(request.body.decode("utf-8")))
            issuer = TokenIssuer.get(name=user.provider)

            valid = issuer.validator.validate(user=user)

            if not valid:
                return HttpResponseBadRequest("Invalid email or token.")

            django_user = User.objects.get_or_create(email=user.email)
            login(request=request, user=django_user)
            django_user.save()
        except json.JSONDecodeError:
            return HttpResponseBadRequest("No authentication data has been sent.")

        except ValueError as exc:
            return HttpResponseBadRequest(str(exc))

        if user.ttl:
            RedisPool()[RedisDB.DB_AUTH].setex(user.email, user.ttl, json.dumps(user.data))

        return HttpResponse()

    def delete(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            RedisPool()[RedisDB.DB_AUTH].delete(request.user.email)
            logout(request=request)
        return HttpResponse()
