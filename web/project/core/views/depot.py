from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Depot
from core.serializers.depot import DepotCreateSerializer, DepotRetrieveSerializer, DepotUpdateSerializer
from core.utils.serializer_mixin import SerializerMixin


class DepotsView(SerializerMixin, ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Depot.objects.all()
    serializer_class = DepotCreateSerializer
    create_serializer_class = DepotCreateSerializer


class DepotView(SerializerMixin, RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "depot_id"
    queryset = Depot.objects.select_related("managed_by").all()
    serializer_class = DepotRetrieveSerializer
    retrieve_serializer_class = DepotRetrieveSerializer
    update_serializer_class = DepotUpdateSerializer
