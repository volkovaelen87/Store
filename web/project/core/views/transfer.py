from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Transfer
from core.utils.serializer_mixin import SerializerMixin
from core.serializers.transfer import (
    TransferCreateSerializer, TransferRetrieveSerializer, TransferListSerializer, TransferUpdateSerializer
)


class TransfersView(SerializerMixin, ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Transfer.objects.all()
    retrieve_serializer_class = TransferListSerializer
    create_serializer_class = TransferCreateSerializer


class TransferView(SerializerMixin, RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "transfer_id"
    queryset = Transfer.objects.all()
    serializer_class = TransferRetrieveSerializer  # TODO: remove as soon as all specific serializers are implemented
    retrieve_serializer_class = TransferRetrieveSerializer
    update_serializer_class = TransferUpdateSerializer
