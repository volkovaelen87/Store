from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Motion
from core.serializers.motion import MotionRetrieveSerializer, MotionCreateSerializer


class MotionsView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Motion.objects.all()
    serializer_class = MotionCreateSerializer


class MotionView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "motion_id"
    queryset = Motion.objects.all()
    serializer_class = MotionRetrieveSerializer
