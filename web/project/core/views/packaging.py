from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Packaging
from core.serializers.packaging import PackagingSerializer


class PackagingsView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Packaging.objects.all()
    serializer_class = PackagingSerializer


class PackagingView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "packaging_id"
    queryset = Packaging.objects.all()
    serializer_class = PackagingSerializer
