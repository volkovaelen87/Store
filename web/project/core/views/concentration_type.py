from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import ConcentrationType
from core.serializers.concentration_type import ConcentrationTypeSerializer


class ConcentrationTypesView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ConcentrationType.objects.all()
    serializer_class = ConcentrationTypeSerializer


class ConcentrationTypeView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "concentration_type_id"
    queryset = ConcentrationType.objects.all()
    serializer_class = ConcentrationTypeSerializer
