from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import Product
from core.serializers.product import ProductSerializer


class ProductsView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProductSerializer

    def get_queryset(self):
        name_part = self.request.GET.get("name")
        if name_part:
            result = Product.objects.filter(name__icontains=name_part.lower())
        else:
            result = Product.objects.all()
        return result


class ProductView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "product_id"
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
