from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView

from core.models import User
from core.serializers.user import UserSerializer


class UsersView(ListAPIView, CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserView(RetrieveAPIView, DestroyAPIView, UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "user_id"
    queryset = User.objects.all()
    serializer_class = UserSerializer
