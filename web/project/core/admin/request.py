from django.contrib import admin

from core.models import Request, RequestTrough


@admin.register(RequestTrough)
class RequestTroughAdmin(admin.ModelAdmin):
    list_display = ("id", "request_id", "product", "amount",)
    fields = ("request", "product", "amount",)


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    fields = ("created_by", "compile",)
