from django.contrib import admin

from core.models import Packaging


@admin.register(Packaging)
class PackagingAdmin(admin.ModelAdmin):
    fields = ("name",)
