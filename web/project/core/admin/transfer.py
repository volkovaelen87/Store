from django.contrib import admin

from core.models import Transfer


@admin.register(Transfer)
class TransferAdmin(admin.ModelAdmin):
    fields = ("organization", "completed_at", "request",)
