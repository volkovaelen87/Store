from django.contrib import admin

from core.models import Department


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    fields = ("name",)
