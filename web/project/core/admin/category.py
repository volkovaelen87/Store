from django.contrib import admin

from core.models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ("name",)
