from django.contrib import admin

from core.models import Depot


@admin.register(Depot)
class DepotAdmin(admin.ModelAdmin):
    fields = [field.name for field in Depot._meta.fields if field.name != "id"]
