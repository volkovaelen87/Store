from django.contrib import admin

from core.models import ConcentrationType


@admin.register(ConcentrationType)
class ConcentrationTypeAdmin(admin.ModelAdmin):
    fields = ("name",)
