from django.contrib import admin

from core.models import Motion


@admin.register(Motion)
class MotionAdmin(admin.ModelAdmin):
    fields = ("depot", "item", "change", "transfer",)
