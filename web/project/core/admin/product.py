from django.contrib import admin

from core.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fields = ("name", "packaging", "package_amount", "concentration_type", "concentration", "category", "details",)
