from core.admin.user import UserAdmin
from core.admin.request import RequestAdmin, RequestTroughAdmin
from core.admin.department import DepartmentAdmin
from core.admin.product import ProductAdmin
from core.admin.packaging import PackagingAdmin
from core.admin.post import PostAdmin
from core.admin.depot import DepotAdmin
from core.admin.motion import MotionAdmin
from core.admin.concentration_type import ConcentrationTypeAdmin
from core.admin.category import CategoryAdmin
from core.admin.item import ItemAdmin
from core.admin.transfer import TransferAdmin
from core.admin.organization import OrganizationAdmin

