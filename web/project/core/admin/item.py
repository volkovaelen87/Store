from django.contrib import admin

from core.models import Item


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    fields = ("product", "serial", "made_date", "expiration_date", "item_cost",)
