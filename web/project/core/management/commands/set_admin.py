from django.core.management.base import BaseCommand, CommandError
from core.models import User


class Command(BaseCommand):
    help = "Sets admin permissions to the specified user."

    def add_arguments(self, parser):
        parser.add_argument('email', type=str)

    def handle(self, *args, **options):
        try:
            user = User.objects.get(email=options["email"])
        except User.DoesNotExist:
            raise CommandError('User "%s" does not exist' % options["email"])

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True

        user.save()

        self.stdout.write(self.style.SUCCESS('Successfully updated user "%s"' % user.email))
