from django.db.models import Sum

from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from core.models import Motion
from core.models.motion import MotionType
from core.models.organization import OrganizationType
from core.serializers.item import ItemDetailSerializer


class MotionListSerializer(ModelSerializer):
    class Meta:
        model = Motion
        fields = "__all__"


class MotionCreateSerializer(ModelSerializer):
    class Meta:
        model = Motion
        fields = ("depot", "type", "item", "change", "transfer",)
        read_only_fields = ("created_at",)

    def validate_depot(self, depot):
        if depot.organization.type == OrganizationType.INTERNAL and depot.managed_by != self.context["request"].user:
            raise ValidationError("Depot {depot_name} does not belong to you.".format(depot_name=depot.name))
        return depot

    def validate(self, attrs):
        depot = attrs["depot"]
        change = attrs["change"]
        motion_type = MotionType(attrs["type"])

        if motion_type == MotionType.LOADING and change <= 0:
            raise ValidationError("Motion of LOADING type should be greater than 0.")

        if motion_type == MotionType.UNLOADING and change >= 0:
            raise ValidationError("Motion of UNLOADING type should be lower than 0.")

        if motion_type == MotionType.UNLOADING and depot.organization.type == OrganizationType.INTERNAL.value:
            item_amount = Motion.objects.filter(
                depot=attrs["depot"],
                item=attrs["item"]
            ).aggregate(sum=Sum('change'))["sum"]
            item_amount = item_amount or 0

            if attrs["change"] > item_amount:
                raise ValidationError("Not enough product '{product_name}' in Depot '{depot_name}'.".format(
                    product_name=attrs["item"].product.name,
                    depot_name=attrs["depot"].name
                ))

        return super().validate(self, attrs=attrs)


class MotionRetrieveSerializer(ModelSerializer):
    item = ItemDetailSerializer()

    class Meta:
        model = Motion
        fields = "__all__"
