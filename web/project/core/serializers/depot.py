from rest_framework.serializers import ModelSerializer

from core.serializers.organisation import OrganizationRetrieveSerializer
from core.serializers.user import UserSerializer

from core.models import Depot


class DepotCreateSerializer(ModelSerializer):
    class Meta:
        model = Depot
        fields = "__all__"


class DepotRetrieveSerializer(ModelSerializer):
    organization = OrganizationRetrieveSerializer(read_only=True)
    managed_by = UserSerializer(read_only=True)

    class Meta:
        model = Depot
        fields = "__all__"


class DepotUpdateSerializer(ModelSerializer):
    class Meta:
        model = Depot
        fields = "__all__"
