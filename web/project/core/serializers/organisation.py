from rest_framework.serializers import ModelSerializer

from core.models import Organization


class OrganizationCreateSerializer(ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"


class OrganizationRetrieveSerializer(ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"
