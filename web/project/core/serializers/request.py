from django.db import transaction
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import CharField, ModelSerializer

from core.models import Request
from core.serializers.request_trough import RequestTroughRetrieveSerializer, RequestTroughCreateSerializer


class RequestListSerializer(ModelSerializer):
    class Meta:
        model = Request
        exclude = ("products",)
        read_only_fields = ("created_at", "created_by",)


class RequestRetrieveSerializer(ModelSerializer):
    products = RequestTroughRetrieveSerializer(many=True, source="requesttrough_set", read_only=True)

    class Meta:
        model = Request
        fields = "__all__"
        read_only_fields = ("created_at", "created_by", "products",)


class RequestCreateSerializer(ModelSerializer):
    products = RequestTroughCreateSerializer(many=True, write_only=True)

    class Meta:
        model = Request
        fields = "__all__"
        read_only_fields = ("created_at", "created_by", "completed_date",)

    @transaction.atomic
    def create(self, validated_data):
        validated_data["created_by"] = self.context["request"].user

        products = validated_data.pop("products")
        if not products:
            raise ValidationError("Products are not clarified.")

        request = super().create(validated_data=validated_data)

        request_through_serializer = RequestTroughCreateSerializer()
        for request_through_data in products:
            request_through_data["request"] = request
            request_through_serializer.create(validated_data=request_through_data)

        return request


class RequestUpdateSerializer(ModelSerializer):
    # TODO: later we should consider about IDLE Users: we should not allow to assign requests to User on sick leave
    # TODO: or vacation
    completed_date = CharField(allow_blank=True)

    class Meta:
        model = Request
        fields = ("completed_date", "compile",)

    def validate_completed_date(self, value):
        if not self.instance:
            raise ValidationError("No instance to update.")

        if self.instance.completed_date:
            raise ValidationError("Request '{id}' is already completed.".format(id=self.instance.id))

        result = None
        if value and self.context["request"].user == self.instance.created_by:
            result = timezone.now()

        return result
