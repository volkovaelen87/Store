from rest_framework.serializers import ModelSerializer, SerializerMethodField, IntegerField

from core.models import Item
from core.serializers.product import ProductSimpleSerializer


class ItemSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = "__all__"


class ItemDetailSerializer(ModelSerializer):
    product = ProductSimpleSerializer()

    class Meta:
        model = Item
        fields = "__all__"


class ItemDetailStorageSerializer(ModelSerializer):
    STORAGE = "storage_"

    product = ProductSimpleSerializer()
    amount = IntegerField()
    per_storage_amount = SerializerMethodField()

    class Meta:
        model = Item
        fields = "__all__"

    def get_per_storage_amount(self, obj):
        result = []
        for key, amount in obj.__dict__.items():
            if key.startswith(self.STORAGE) and amount:
                result.append({
                    "storage_id": int(key.replace(self.STORAGE, "")),
                    "amount": amount
                })
        return result
