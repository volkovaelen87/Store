from rest_framework.serializers import ModelSerializer

from core.models import ConcentrationType


class ConcentrationTypeSerializer(ModelSerializer):
    class Meta:
        model = ConcentrationType
        fields = "__all__"
