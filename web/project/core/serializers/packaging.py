from rest_framework.serializers import ModelSerializer

from core.models import Packaging


class PackagingSerializer(ModelSerializer):
    class Meta:
        model = Packaging
        fields = "__all__"
