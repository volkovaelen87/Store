from rest_framework.serializers import ModelSerializer, CharField

from core.models import Product
from core.serializers.category import CategorySerializer
from core.serializers.concentration_type import ConcentrationTypeSerializer
from core.serializers.packaging import PackagingSerializer


class ProductSimpleSerializer(ModelSerializer):
    concentration_name = CharField(source="concentration_type.name", read_only=True)
    packaging_name = CharField(source="packaging.name", read_only=True)

    class Meta:
        model = Product
        fields = ["id", "concentration_name", "name", "package_amount", "concentration", "details", "packaging_name", ]


class ProductSerializer(ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    concentration_type = ConcentrationTypeSerializer(read_only=True)
    packaging = PackagingSerializer()

    class Meta:
        model = Product
        fields = "__all__"
