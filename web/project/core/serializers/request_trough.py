from rest_framework.serializers import ModelSerializer

from core.models import RequestTrough
from core.serializers.product import ProductSerializer


class RequestTroughRetrieveSerializer(ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = RequestTrough
        exclude = ("id", "request",)


class RequestTroughCreateSerializer(ModelSerializer):
    class Meta:
        model = RequestTrough
        exclude = ("request",)
