from django.db.models import Sum
from django.db import transaction
from django.utils import timezone

from rest_framework.serializers import ModelSerializer, Serializer, PrimaryKeyRelatedField
from rest_framework.exceptions import ValidationError

from core.models import Transfer, Motion, Depot
from core.models.motion import MotionType
from core.models.organization import OrganizationType

from core.serializers.motion import (
    MotionCreateSerializer, MotionRetrieveSerializer
)


class TransferListSerializer(ModelSerializer):
    class Meta:
        model = Transfer
        fields = "__all__"
        read_only_fields = ("created_at", "completed_at",)


class TransferMotionInputSerializer(ModelSerializer):
    class Meta:
        model = Motion
        fields = ("depot", "item", "change",)

    def validate_change(self, value):
        if value <= 0:
            raise ValidationError("Change should be higher than zero, got, '{}'".format(value))
        return -value


class TransferCreateSerializer(ModelSerializer):
    motions = TransferMotionInputSerializer(source="motion_set", many=True)

    class Meta:
        model = Transfer
        fields = "__all__"
        read_only_fields = ("created_at", "completed_at",)

    def validate_motions(self, value):
        user = self.context["request"].user

        if not value:
            raise ValidationError("Motions are not clarified.")

        # Check that all motions belong to the one organization
        organization = value[0]["depot"].organization
        for motion in value:
            if organization.id != motion["depot"].organization_id:
                raise ValidationError("All unloading Depots should belong to the same organization.")

        # If UNLOADING organization is INTERNAL all depots should belong to the current user
        if organization.type == OrganizationType.INTERNAL.name:
            for motion in value:
                if motion["depot"].managed_by_id != user.id:
                    raise ValidationError("You are trying to unload products from not your Depot: '{name}'.".format(
                        name=motion["depot"].name
                    ))

        return value

    def validate(self, attrs):
        # If source Organization is EXTERNAL then destination Organization should be INTERNAL
        if attrs["motion_set"][0]["depot"].organization.type == OrganizationType.EXTERNAL.name:
            if attrs["organization"].type == OrganizationType.EXTERNAL.name:
                raise ValidationError("Source and destination organizations could not be EXTERNAL.")

        # Verify that user belongs to source or destination organization
        source_organization_id = attrs["motion_set"][0]["depot"].organization_id
        destination_organization_id = attrs["organization"].id

        receiver_organization_id = attrs["receiver"].organization_id

        if receiver_organization_id not in (source_organization_id, destination_organization_id,):
            raise ValidationError("Receiver should belong to the source or destination organization.")

        return super().validate(attrs=attrs)

    @transaction.atomic
    def create(self, validated_data):
        motions = validated_data.pop("motion_set")
        transfer = super().create(validated_data=validated_data)

        internal_source = True
        if motions[0]["depot"].organization.type == OrganizationType.EXTERNAL.name:
            internal_source = False

        motion_create_serializer = MotionCreateSerializer()
        for motion in motions:
            motion["transfer"] = transfer
            motion["type"] = MotionType.UNLOADING.value
            motion_create_serializer.create(validated_data=motion)

        if internal_source:
            for motion in motions:
                item_amount = Motion.objects.filter(
                    depot=motion["depot"],
                    item=motion["item"]
                ).aggregate(sum=Sum('change'))["sum"]

                item_amount = item_amount or 0

                if item_amount < 0:
                    raise ValidationError("Not enough '{product}' product amount in Depot: '{depot_name}'".format(
                        product=motion["item"].product.name,
                        depot_name=motion["depot"].name
                    ))

        return transfer


class TransferRetrieveSerializer(ModelSerializer):
    motions = MotionRetrieveSerializer(source="motion_set", many=True, read_only=True)

    class Meta:
        model = Transfer
        fields = "__all__"


class TransferUpdateMotionSerializer(Serializer):
    motion = PrimaryKeyRelatedField(queryset=Motion.objects.all())
    depot = PrimaryKeyRelatedField(queryset=Depot.objects.all())


class TransferUpdateSerializer(ModelSerializer):
    motions = TransferUpdateMotionSerializer(many=True, write_only=True)

    class Meta:
        model = Transfer
        fields = ("motions",)

    def validate_motions(self, value):
        if not self.instance:
            raise ValidationError("No instance to update.")

        # Get id's of all UNLOADING Motions
        required_motions_mapping = {m.id for m in self.instance.motion_set.filter(type=MotionType.UNLOADING.value)}

        current_user = self.context["request"].user
        received_motions = []
        destination_set = set()
        for mapping in value:
            received_motions.append(mapping["motion"].id)

            motion_depot = mapping["depot"]

            if motion_depot.managed_by_id != current_user.id:
                raise ValidationError("Depot {depot_name} does not belong to you.".format(
                    depot_name=motion_depot.name
                ))

            if self.instance.organization_id != motion_depot.organization_id:
                raise ValidationError("All destination Depots should belong to the '{name}' organization.".format(
                    name=self.instance.organization.name
                ))

            # Add item, depot to the destination_set
            destination_set.add((mapping["motion"].item_id, mapping["depot"].id,))

        if len(received_motions) != len(required_motions_mapping):
            raise ValidationError("Items mapping is incompatible.")

        if set(received_motions) != required_motions_mapping:
            raise ValidationError("Not all items has been mapped.")

        # Get id's depot and item LOADING Motions
        source_set = set(
            [
                (motion.item_id, motion.depot_id)
                for motion in self.instance.motion_set.filter(type=MotionType.UNLOADING.value)
            ]
        )

        if source_set & destination_set:
            raise ValidationError("You are trying to unload and load the same Item to the same Depot.")

        return value

    def validate(self, attrs):
        user = self.context["request"].user
        if self.instance.receiver_id != user.id:
            required_user = self.instance.receiver
            raise ValidationError("This request could be completed by {fn} {ln} ({email}) only.".format(
                fn=required_user.first_name,
                ln=required_user.last_name,
                email=required_user.email
            ))
        return super().validate(attrs=attrs)

    @transaction.atomic
    def update(self, instance, validated_data):
        if instance.completed_at:
            raise ValidationError("Transfer '{id}' is already completed.".format(id=instance.id))

        instance.completed_at = timezone.now()
        instance.save()

        motions_create_serializer = MotionCreateSerializer()
        for motion_data in validated_data["motions"]:
            source_motion = motion_data.pop("motion")
            motion_data["type"] = MotionType.LOADING.value
            motion_data["item"] = source_motion.item
            motion_data["change"] = -source_motion.change
            motion_data["transfer"] = instance
            motions_create_serializer.create(validated_data=motion_data)

        return instance
