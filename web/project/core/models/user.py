from django.db import models
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser

from core.models import Organization


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, **extra_fields):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        extra_fields["is_staff"] = True
        extra_fields["is_superuser"] = True

        return self.create_user(
            email=email.lower(),
            **extra_fields
        )

    def get_or_create(self, email: str, *args, **kwargs):
        email = email.lower()
        try:
            return self.get(email=email)
        except User.DoesNotExist:
            return self.create_user(email=email)


class User(PermissionsMixin, AbstractBaseUser):
    email = models.EmailField(
        max_length=255,
        unique=True
    )

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    creation_date = models.DateTimeField(auto_now=True)
    last_login = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    is_staff = models.BooleanField(default=False)

    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def user_email(self):
        return self.email

    def get_username(self):
        return self.user_email

    def __str__(self):
        return "{fn} {ln} ({email})".format(
            fn=self.first_name,
            ln=self.last_name,
            email=self.email
        )
