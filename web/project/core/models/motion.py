from django.db import models

from core.models import Item, Depot, Transfer
from core.utils import NotZeroValidator

from core.utils.django_choices_enum import DjangoChoicesEnum


class MotionType(DjangoChoicesEnum):
    LOADING = "loading"
    UNLOADING = "unloading"


class Motion(models.Model):
    depot = models.ForeignKey(Depot, on_delete=models.PROTECT)
    transfer = models.ForeignKey(Transfer, on_delete=models.PROTECT)
    type = models.CharField(max_length=15, choices=MotionType.django_choices())

    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    change = models.IntegerField(validators=[NotZeroValidator()])

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{depot} - {item} #{amount} ({date})".format(
            depot=str(self.depot),
            item=str(self.item),
            amount=self.change,
            date=self.created_at
        )
