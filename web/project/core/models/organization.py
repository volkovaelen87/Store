from django.db import models
from django.core.validators import MinLengthValidator

from core.utils import DjangoChoicesEnum


class OrganizationType(DjangoChoicesEnum):
    INTERNAL = "internal"
    EXTERNAL = "external"


class Organization(models.Model):
    name = models.CharField(
        unique=True,
        max_length=100,
        validators=[MinLengthValidator(2, message="Organisation name should be at least 3 characters.")]
    )
    type = models.CharField(max_length=10, choices=OrganizationType.django_choices())

    address = models.CharField(blank=True, max_length=200)
    tin = models.CharField(verbose_name="Taxpayer identification number", max_length=50, unique=True)

    def __str__(self):
        return self.name
