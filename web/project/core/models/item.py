from django.core.validators import MinValueValidator
from django.db import models

from core.models import Product


class Item(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    serial = models.CharField(max_length=32, blank=False)
    made_date = models.DateField(verbose_name="creation date", blank=False)
    expiration_date = models.DateField(verbose_name="expiration date", blank=False)
    item_cost = models.FloatField(validators=[MinValueValidator(0)])

    def __str__(self):
        # TODO: clarify
        return "{product} NN {serial} ({item_cost}) ({creation_date}/{expiration_date})".format(
            product=str(self.product),
            serial=str(self.serial),
            item_cost=self.item_cost,
            creation_date=self.made_date,
            expiration_date=self.expiration_date,
        )
