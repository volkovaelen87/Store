from django.core.validators import MinLengthValidator
from django.db import models


class Category(models.Model):
    name = models.CharField(
        unique=True,
        max_length=100,
        validators=[MinLengthValidator(3, message="Product category name should be at least 3 characters.")]
    )

    def __str__(self):
        return self.name
