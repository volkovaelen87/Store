from django.db import models
from django.core.validators import MinLengthValidator


class Department(models.Model):
    name = models.CharField(
        unique=True,
        max_length=30,
        validators=[MinLengthValidator(3, message="Department name should be at least 3 characters.")]
    )

    def __str__(self):
        return self.name
