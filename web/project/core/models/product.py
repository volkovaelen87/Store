from django.core.validators import MinLengthValidator, MinValueValidator
from django.db import models

from core.models import Packaging, ConcentrationType, Category


class Product(models.Model):
    class Meta:
        unique_together = ("name", "packaging", "package_amount", "concentration_type", "concentration",)

    name = models.CharField(
        max_length=50,
        validators=[MinLengthValidator(3, message="Product name should be at least 3 characters.")]
    )

    packaging = models.ForeignKey(Packaging, on_delete=models.PROTECT)
    package_amount = models.IntegerField(verbose_name="amount in package", validators=[MinValueValidator(0)])

    concentration_type = models.ForeignKey(ConcentrationType, on_delete=models.PROTECT)
    concentration = models.FloatField(validators=[MinValueValidator(0)])

    category = models.ManyToManyField(Category)
    details = models.TextField(blank=True)

    def __str__(self):
        return "{name} ({packaging}), {concentration} {ctype}".format(
            name=self.name,
            packaging=self.packaging,
            concentration=self.concentration,
            ctype=str(self.concentration_type.name)
        )
