from django.db import models
from django.core.validators import MinLengthValidator

from core.models import User
from core.models import Organization


class Depot(models.Model):
    name = models.CharField(
        unique=True,
        max_length=30,
        validators=[MinLengthValidator(3, message="Depot name should be at least 3 characters.")]
    )
    address = models.CharField(blank=True, max_length=200)
    managed_by = models.ForeignKey(User, on_delete=models.PROTECT)

    organization = models.ForeignKey(Organization, max_length=200, on_delete=models.PROTECT)

    def __str__(self):
        return self.name
