from django.core.validators import MinValueValidator
from django.db import models

from core.models import User, Product


class Request(models.Model):
    created_by = models.ForeignKey(User, related_name="created_by", on_delete=models.PROTECT)
    compile = models.ForeignKey(User, related_name="compile", on_delete=models.PROTECT)

    products = models.ManyToManyField(Product, through="RequestTrough")

    created_at = models.DateTimeField(auto_now_add=True)

    completed_date = models.DateTimeField(null=True, default=None)

    def __str__(self):
        return "{created_by} ({date}/{completed_date})".format(
            created_by=str(self.created_by),
            date=self.created_at,
            completed_date=self.completed_date
        )


class RequestTrough(models.Model):
    request = models.ForeignKey(Request, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    amount = models.IntegerField(validators=[MinValueValidator(1)])

    def __str__(self):
        return "Request '{request_id}', Product '{product}', amount {amount}".format(
            request_id=self.request_id,
            product=str(self.product),
            amount=self.amount
        )
