from django.db import models

from core.models import Request, User, Organization


class Transfer(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    completed_at = models.DateTimeField(blank=True, null=True)
    request = models.ForeignKey(Request, on_delete=models.PROTECT, null=True)
    receiver = models.ForeignKey(User, on_delete=models.PROTECT)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, verbose_name="destination organization")

    def __str__(self):
        return "({started}-{finished})".format(
            started=self.created_at,
            finished=self.completed_at
        )
