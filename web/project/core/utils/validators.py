from django.core.validators import BaseValidator
from django.utils.translation import gettext_lazy as _

from django.utils.deconstruct import deconstructible


@deconstructible
class NotZeroValidator(BaseValidator):
    message = _("Ensure this value is not equal to 0.")
    code = 'non_zero_value'

    def __init__(self, message=None):
        super().__init__(0, message=message)

    def compare(self, a, b):
        return a == b
