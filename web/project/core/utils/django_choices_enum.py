from enum import Enum


class DjangoChoicesEnum(Enum):

    @classmethod
    def django_choices(cls):
        return tuple([(item.name, item.value,) for item in list(cls)])
