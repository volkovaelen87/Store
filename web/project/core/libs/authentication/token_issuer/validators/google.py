import json
from urllib.request import urlopen

from .base_token_validator import BaseTokenValidator


class GoogleTokenValidator(BaseTokenValidator):
    EMAIL_KEY = "email"

    EXPIRES = "expires_in"

    URL = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={}"

    def validate(self, user) -> bool:
        if not user.email:
            return False

        try:
            data = urlopen(self.URL.format(user.access_token)).read()
            data = json.loads(data.decode("utf-8"))

            # Check email
            if data[self.EMAIL_KEY].lower() != user.email.lower():
                return False

        except (json.JSONDecodeError, KeyError):
            return False

        user.ttl = int(data.get(self.EXPIRES))
        return True
