from abc import abstractmethod

from core.utils.singleton import ABCSingleton


class BaseTokenValidator(metaclass=ABCSingleton):

    @abstractmethod
    def validate(self, user) -> bool:
        pass
