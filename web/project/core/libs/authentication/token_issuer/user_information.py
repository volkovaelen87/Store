class UserInformation:
    def __init__(self, data):
        self.__ttl = None
        self.__data = data

    @property
    def provider(self):
        return self.__data["provider"]

    @property
    def data(self):
        return self.__data

    @property
    def email(self):
        return self.__data["email"]

    @property
    def access_token(self):
        return self.__data["accessToken"]

    @property
    def ttl(self):
        return self.__ttl

    @ttl.setter
    def ttl(self, value):
        self.__ttl = value
