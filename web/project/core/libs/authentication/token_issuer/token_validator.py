from enum import Enum

from .validators.google import GoogleTokenValidator


class TokenIssuer(Enum):
    GOOGLE = ("google", GoogleTokenValidator())

    def __init__(self, name, validator):
        self._name_ = name
        self._validator = validator

    @classmethod
    def get(cls, name: str):
        if not name:
            raise ValueError("Token issuer was not clarified.")

        name = name.lower()
        for enum in cls.__members__.values():
            if enum.name == name:
                return enum
        raise ValueError("{name} is not valid for {class_name}".format(
            name=name,
            class_name=cls.__name__
        ))

    @property
    def validator(self):
        return self._validator
