from enum import Enum
from redis import StrictRedis

from django.conf import settings

from core.utils.singleton import Singleton


class RedisDB(Enum):
    DB_CACHE = 0
    DB_AUTH = 3


class RedisPool(metaclass=Singleton):

    def __init__(self):
        self.__connections = {}

    def __getitem__(self, item: RedisDB):
        return self.__connections.setdefault(item, StrictRedis(
            host=settings.REDIS["host"],
            port=settings.REDIS["port"],
            db=item.value
        ))
